import os


def find_pdf(path):
    pdfs = []
    for dirpath, dirnames, filenames in os.walk(path):
        for filename in [f for f in filenames if f.endswith(".pdf")]:
            pdfs.append(os.path.join(dirpath,filename))
    print(pdfs)
    return pdfs


if __name__ == "__main__":
    find_pdf('.')
